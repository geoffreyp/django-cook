from django.shortcuts import render
from recipes.models import Recipe


def index(request):
    recipes = Recipe.objects.filter(status=1).order_by('-created_at')[:3]
    return render(request, 'core/homepage.html', {'recipes': recipes})

