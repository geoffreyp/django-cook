from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User


class UserForm(ModelForm):
	class Meta:
		model = User
		fields = ['username', 'password', 'email']
		widgets = {
			'password': forms.PasswordInput(),
		}
		labels = {
			'username': 'Pseudo',
			'password': 'Mot de passe',
			'email': 'Adresse email',
		}
		help_texts = {
			'username': 'champ obligatoire',
			'password': 'champ obligatoire',
			'email': 'champ obligatoire',
			}
		required = {}
