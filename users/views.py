from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import UserForm

# Use to redirect if the user is logged
def not_loggedin_required(function):
	def wrap(request, *args, **kwargs):
		if request.user.is_authenticated:
			return redirect("/")  # redirect to profile page
		else:
			return function(request, *args, **kwargs)

	return wrap


@not_loggedin_required
def register(request):
	if request.method == 'POST':
		form = UserForm(request.POST)

		if form.is_valid():
			User.objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])
			return HttpResponseRedirect('/account/login')
	else:
		form = UserForm

	return render(request, 'users/register.html', {'form': form})
