# Django project for Django course in Master I2L

## Install

Installation des dépendances
```
pip install -r requirements.txt
```

Installation de la base de données
```
python manage.py migrate
```

Import des données de test
```
python manage.py loaddata demo.json
```


## Libs
 - django-bootstrap4 http://django-bootstrap4.readthedocs.io/en/latest/index.html

## Architecture
- core app : specific app for the project
- users app : manage user system
- recipes app : manage cook recipes

## Système de méta-info
 - administrable dans l'admin (on peut ajouter facilement des nouveaux type (valeur nutritionnelle, la saison, ...))
 - inspiré de l'architecture de wordpress
    - Méta-type (exemple : ingrédient, couleur, difficulé)
    - Méta-data (exemple : pomme, rouge, facile)
    - Recipe-metadata (fait le lien entre les meta et les recettes)
 - toutes les métas data sont utilisé dans le système de recherche, on peut donc chercher les recettes faciles ou avec des couleurs vertes

## Gestion de projet 

https://gitlab.com/geoffreyp/django-cook/boards?=