from django.contrib import admin

from recipes.models import Recipe, MetaType, MetaData, RecipeMetaData

admin.site.register(MetaType)
admin.site.register(MetaData)
admin.site.register(RecipeMetaData)
admin.site.register(Recipe)
