from django.forms import ModelForm, Form, formset_factory
from django import forms
from recipes.models import Recipe, Comment, RecipeMetaData, MetaType, MetaData


class RecipeStep1Form(ModelForm):
	class Meta:
		model = Recipe
		fields = ['name', 'time_preparation', 'time_cooking', 'time_rest', 'cost', 'steps', 'picture']
		labels = {
			'name': 'Nom de votre recette',
			# 'ingredients': 'Liste des ingrédients nécessaires',
			'time_preparation': 'Temps de préparation',
			'time_cooking': 'Temps de cuisson',
			'time_rest': 'Temps de repos',
			'cost': 'Coût',
			'steps': 'Etapes de préparation',
			'picture': 'photo de votre plat',
		}
		help_texts = {
		 	'time_preparation': 'En minutes',
			'time_cooking': 'En minutes',
			'time_rest': 'En minutes',
			'cost': 'En euros',
		 	}
		required = {}


def load_select_type():
	tpl = (('', 'Choisissez une info'),)
	for i in range(300):
		tpl = tpl + ((str(i), ''),)
	return tpl


def load_value_type():
	tpl = (('', 'Choisissez une valeur'),)
	for i in range(300):
		tpl = tpl + ((str(i), ''),)
	return tpl


class RecipeStep2Form(Form):
	select_type = forms.ChoiceField(widget=forms.Select, choices=load_select_type())
	select_type.widget.attrs.update({'class': 'form-control'})
	select_type.widget.attrs.update({'required': 'required'})

	value_type = forms.ChoiceField(widget=forms.Select, choices=load_value_type())
	value_type.widget.attrs.update({'class': 'form-control'})
	value_type.widget.attrs.update({'required': 'required'})


RecipeStep2Formset = formset_factory(RecipeStep2Form, extra=1)


class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ['text']
		labels = {'text': 'Votre message'}
