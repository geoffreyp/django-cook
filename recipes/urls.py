from django.urls import path

from recipes import views

app_name = 'recipes'
urlpatterns = [
    path('', views.index, name='index'),
    path('ajax/meta/type', views.meta_type_html, name='ajax_meta_type'),
    path('ajax/meta/data', views.meta_data_html, name='ajax_meta_data'),
    path('create/1', views.create, name='create'),
    path('create/2/recipe/<int:recipe_id>/', views.create_step2, name='create_step2'),
    path('edit/1/recipe/<int:recipe_id>/', views.edit, name='edit'),
    path('edit/2/recipe/<int:recipe_id>/', views.edit_step2, name='edit_step2'),
    path('delete/<int:recipe_id>/', views.delete, name='delete'),
    path('<int:recipe_id>', views.detail, name='detail'),
    path('<int:recipe_id>/comment', views.new_comment, name='comment'),
    path('<int:recipe_id>/score', views.vote_score, name='score'),
    path('search', views.full_search, name='search'),
]
