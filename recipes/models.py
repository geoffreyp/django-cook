from django.db import models
from datetime import datetime, timedelta
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from django.conf import settings


class Recipe(models.Model):
	name = models.CharField(max_length=100)
	time_preparation = models.IntegerField(default=1, validators=[MinValueValidator(1)])
	time_cooking = models.IntegerField(default=1, validators=[MinValueValidator(1)])
	time_rest = models.IntegerField(default=1, validators=[MinValueValidator(1)])
	cost = models.IntegerField(default=1, validators=[MinValueValidator(1)])
	steps = models.TextField()
	picture = models.ImageField(upload_to = 'pictures/', default = 'pictures/no-img.png')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)
	status = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(1)])
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	def was_published_recently(self):
		return self.created_at >= timezone.now() - timedelta(days=1)

	def is_enabled(self):
		return self.status == 1

	def __str__(self):
		return self.name

	def url(self):
		return "/recipes/" + str(self.id)

	def score_mean(self):
		scores = Score.objects.filter(recipe=self)
		if scores.count() == 0:
			return 0
		else:
			res = 0
			for score in scores:
				res += score.value

			return float(res/scores.count())


class MetaType(models.Model):
	type = models.CharField(max_length=100)
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class MetaData(models.Model):
	value = models.CharField(max_length=250)
	type = models.ForeignKey("MetaType", on_delete=models.PROTECT, related_name="meta_type")

	def __str__(self):
		return "[" + self.type.name + "] " + self.value


class RecipeMetaData(models.Model):
	recipe = models.ForeignKey("Recipe", on_delete=models.PROTECT, related_name="recipe")
	meta = models.ForeignKey("MetaData", on_delete=models.PROTECT, related_name="recipe_meta")

	def __str__(self):
		return "[" + self.recipe.name + "] " + self.meta.value


class Comment(models.Model):
	text = models.TextField()
	likes = models.IntegerField(default=0, validators=[MinValueValidator(0)])
	created_at = models.DateTimeField(auto_now_add=True)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
	recipe = models.ForeignKey("Recipe", on_delete=models.CASCADE)

	def __str__(self):
		return self.text


class Score(models.Model):
	value = models.IntegerField(default=0, validators=[MinValueValidator(0)])
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name="author_score")
	recipe = models.ForeignKey("Recipe", on_delete=models.PROTECT, related_name="recipe_score")

	class Meta:
		unique_together = (("author", "recipe"),)

	def __str__(self):
		return str(self.value)
