from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.core.paginator import Paginator
from django.http import Http404, HttpResponse, HttpResponseForbidden
import recipes, core
from django.urls import reverse

from .forms import RecipeStep1Form, CommentForm, RecipeStep2Formset
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader

from recipes.models import Recipe, Comment, Score, RecipeMetaData, MetaType, MetaData


def index(request):
	recipes_list = Recipe.objects.filter(status=1)
	paginator = Paginator(recipes_list, 3)
	page = request.GET.get('page')

	recipes = paginator.get_page(page)

	return render(request, 'recipes/index.html', {'recipes': recipes})

@login_required
def delete(request, recipe_id):
	recipe = get_object_or_404(Recipe, id=recipe_id)

	if recipe.author != request.user:
		#return HttpResponseForbidden("Vous n'êtes pas autorisé à modifier cette recette")
		template = loader.get_template('error/403.html')
		return HttpResponseForbidden(
        template.render(request=request)
    )

	recipe.status = 0
	recipe.save()
	return redirect('recipes:index')
	


@login_required
def edit(request, recipe_id):
	recipe = get_object_or_404(Recipe, id=recipe_id)

	if recipe.author != request.user:
		#return HttpResponseForbidden("Vous n'êtes pas autorisé à modifier cette recette")
		template = loader.get_template('error/403.html')
		return HttpResponseForbidden(
        template.render(request=request)
    )

	if request.POST:
		form = RecipeStep1Form(request.POST, request.FILES, instance=recipe)
		if form.is_valid():
			recipe.save()
		return redirect('recipes:edit_step2', recipe.id)
	else:
		form = RecipeStep1Form(instance=recipe)
	return render(request, 'recipes/edit_step1.html', {'form': form, 'id': recipe_id})


@login_required
def edit_step2(request, recipe_id):
	recipe = get_object_or_404(Recipe, id=recipe_id)

	if request.POST:
		formset = RecipeStep2Formset(request.POST, instance=recipe)
		if form.is_valid():
			for form in formset:
				metadata = get_object_or_404(MetaData, pk=form.cleaned_data.get('value_type'))

				recipe_metadata = RecipeMetaData()
				recipe_metadata.meta = metadata
				recipe_metadata.recipe = recipe
				recipe_metadata.save()
			recipe.status = 1
			recipe.save()
			return redirect('recipes:index')
	else:
		formset = RecipeStep2Formset()
	return render(request, 'recipes/create_step2.html', {'formset': formset, 'recipe': recipe})

@login_required
def create(request):
	new_recipe = Recipe()
	if request.POST:
		form = RecipeStep1Form(request.POST, request.FILES)
		if form.is_valid():
			new_recipe.name = form.cleaned_data['name']
			new_recipe.time_preparation = form.cleaned_data['time_preparation']
			new_recipe.time_cooking = form.cleaned_data['time_cooking']
			new_recipe.time_rest = form.cleaned_data['time_rest']
			new_recipe.cost = form.cleaned_data['cost']
			new_recipe.steps = form.cleaned_data['steps']
			new_recipe.picture = form.cleaned_data['picture']
			new_recipe.author = request.user
			new_recipe.save()
		return redirect('recipes:create_step2', new_recipe.id)
	else:
		form = RecipeStep1Form
	return render(request, 'recipes/create_step1.html', {'form': form})


@login_required
def create_step2(request, recipe_id):
	print(recipe_id)
	recipe = get_object_or_404(Recipe, id=recipe_id)
	print(recipe.id)

	if request.POST:
		formset = RecipeStep2Formset(request.POST)

		if formset.is_valid():
			for form in formset:
				metadata = get_object_or_404(MetaData, pk=form.cleaned_data.get('value_type'))

				recipe_metadata = RecipeMetaData()
				recipe_metadata.meta = metadata
				recipe_metadata.recipe = recipe
				recipe_metadata.save()

			recipe.status = 1
			recipe.save()
			return redirect('recipes:index')
	else:
		formset = RecipeStep2Formset()

	return render(request, "recipes/create_step2.html", {'formset': formset, 'recipe': recipe})


def detail(request, recipe_id):
	recipe = get_object_or_404(Recipe, id=recipe_id)

	comments_list = Comment.objects.filter(recipe=recipe)
	paginator = Paginator(comments_list, 3)
	page = request.GET.get('page')

	comments = paginator.get_page(page)

	metas = RecipeMetaData.objects.filter(recipe=recipe)
	ingredients = []
	for meta in metas:
		if meta.meta.type.type == 'ingredient':
			ingredients.append(meta.meta)	
	form = CommentForm()
	if request.user.is_authenticated:
		score = Score.objects.filter(recipe_id=recipe_id, author=request.user).first()	
	else:
		score = 0
	score_mean = recipe.score_mean()

	if score:
		value = score.value
	else:
		value = 0

	return render(request, 'recipes/detail.html', {'recipe': recipe, 'comments': comments, 'score': value, 'score_mean': score_mean, 'form': form, 'ingredients': ingredients})


@login_required
def new_comment(request, recipe_id):
	if request.POST:
		form = CommentForm(request.POST)
		comment = Comment()
		if form.is_valid():
			comment.text = form.cleaned_data['text']
			comment.likes = 0
			comment.recipe = Recipe.objects.get(pk=recipe_id)
			comment.author = request.user
			comment.save()
			return redirect('recipes:detail', recipe_id)
	else:
		raise Http404("Not found")


@login_required
def vote_score(request, recipe_id):
	if request.POST:
		recipe = Recipe.objects.get(pk=recipe_id)
		value = request.POST["score"]
		score = Score.objects.filter(recipe=recipe, author=request.user).first()

		if score:
			score.value = value
		else:
			score = Score()
			score.value = value
			score.author = request.user
			score.recipe = recipe

		score.save()

		return HttpResponse("ok")
	else:
		raise Http404("Not found")


def meta_type_html(request):
	metas = MetaType.objects.all()
	return render(request, "recipes/ajax_select_meta_type.html", {"types": metas})


def meta_data_html(request):
	meta_type = get_object_or_404(MetaType, pk=request.GET.get("type_id"))
	metas = MetaData.objects.filter(type=meta_type)

	return render(request, "recipes/ajax_select_meta_data.html", {"data": metas})


def full_search(request):
	recipes = []
	if request.GET.get("key"):
		meta_data = MetaData.objects.filter(value__contains=request.GET.get("key")).values_list('id', flat=True)
		meta_data_recipe_ids = RecipeMetaData.objects.filter(meta__in=meta_data).values_list('recipe', flat=True)

		recipes = Recipe.objects.filter(name__contains=request.GET.get("key")) | Recipe.objects.filter(
			id__in=meta_data_recipe_ids)
	else:
		recipes = Recipe.objects.all()

	for recipe in recipes:
		metas = RecipeMetaData.objects.filter(recipe=recipe)
		ingredients = []
		for meta in metas:
			if meta.meta.type.type == 'ingredient':
				ingredients.append(meta.meta.value)
		recipe.ingredients = ingredients

	return render(request, "recipes/search.html", {"recipes": recipes})